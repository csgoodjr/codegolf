# CodeGolf
This is a repo for a tracking/leaderboard system for a custom code-golf event!
This script uses the code.golf site to grab the specific users to check and score. 

# Default Puzzles
Currently, the default puzzles to solve for are:
- '99-bottles-of-beer'
- 'evil-numbers'
- 'fibonacci'
- 'kolakoski-sequence'
- 'prime-numbers'
- 'niven-numbers'
- 'odious-numbers'
- 'abundant-numbers'
- 'christmas-trees'
- 'leyland-numbers'
- 'look-and-say'
- 'happy-numbers'
- 'tongue-twisters'
- 'sierpiński-triangle'
- 'rock-paper-scissors-spock-lizard'

# Scoring Guidelines
There is logic in the system which uses a specific time-string to determine the times of the events. Additionally, it uses a default list of students and puzzles to determine the final score. Each score is based off of a 500 point 'Par' value, so each puzzle defaults for 500 points, and anything below that is where the real points are awarded.
Additionally, the scores are also based upon the language and overall minimum -- essentially, the minimum score per language is chosen and then is filtered down by the specific 'hole'.