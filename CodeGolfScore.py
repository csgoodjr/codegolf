import collections
import datetime
import json
import requests


students = [
    'Lydxn',
    'hallvabo',
    'Haksell',
    'csg67',
    'razibaig'
]
holes = [
    '99-bottles-of-beer',
    'evil-numbers',
    'fibonacci',
    'kolakoski-sequence',
    'prime-numbers',
    'niven-numbers',
    'odious-numbers',
    'abundant-numbers',
    'christmas-trees',
    'leyland-numbers',
    'look-and-say',
    'happy-numbers',
    'tongue-twisters',
    'sierpiński-triangle',
    'rock-paper-scissors-spock-lizard'
]


def getScores():
    '''
    Gets the scores from the code.golf API
    Returns a prompt of the calls status and the accompanying scores
    '''
    scores = requests.get('https://code.golf/scores/all-holes/all-langs/all')
    if scores.status_code == 200:
        scoreObj = scores.json()
        return f"Success with {len(scoreObj)} scores", scoreObj
    else:
        return f"Failed with code {scores.status_code}", {}


def getDateTime(time):
    return datetime.datetime.strptime(time, "%Y-%m-%dT%H:%M:%S.%f")


def filterScores(student, filter, filterVal, scores):
    '''
    Filters the return object from the code.golf API by a given filter and value
    Also accounts for the student's login
    '''
    studentScores = []
    for score in scores:
        if score['login'] == student and score[filter] == filterVal:
            if getDateTime(score['submitted']) > datetime.datetime(2021, 7, 22, 13, 00, 00, 000) and getDateTime(score['submitted']) < datetime.datetime(2021, 7, 22, 14, 25, 00, 000):
                studentScores.append(score)
    return studentScores


def findMinScore(student, hole, scores):
    minScoreByLang = {}
    for i in filterScores(student, 'hole', hole, scores):
        try:
            minScoreByLang[i['lang']].append(i[i['scoring']])
        except:
            minScoreByLang[i['lang']] = [i[i['scoring']]]
    for key in minScoreByLang.keys():
        minScoreByLang[key] = min(minScoreByLang[key])
    return minScoreByLang


def scoreTotals(scoresByStudents):
    totals = {}
    for student in scoresByStudents.keys():
        totals[student] = 0
        for hole in scoresByStudents[student].keys():
            totals[student] += scoresByStudents[student][hole]
    return totals


if __name__ == "__main__":
    returnCode, scores = getScores()
    # print(scores)

    if "Success" in returnCode and len(scores) > 0:
        minScoreByStudent = {}
        for student in students:
            scoreByHoles = {}
            for hole in holes:
                minScoreByLang = findMinScore(student, hole, scores)
                minScore = 500  # Par Value (Arbitrary)
                if len(minScoreByLang.keys()) > 0:
                    minScore = min([minScoreByLang[i]
                                    for i in minScoreByLang.keys()])
                scoreByHoles[hole] = minScore
            minScoreByStudent[student] = scoreByHoles
        print("LEADERBOARD:")
        for place, (student, score) in enumerate(sorted(scoreTotals(minScoreByStudent).items(),
                                                        key=lambda item: item[1])):
            print(f"Place: {place + 1} --- {student} --- {score}!")
    else:
        print("Something went wrong while trying to process the scores!")
